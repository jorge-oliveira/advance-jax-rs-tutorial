package main.java.pt.jorge.javabrains.rest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Calendar;
import java.util.Date;

// @Path("{pathParam}/test") // http://localhost:8083/myApp/webapi/value/test?query= queryValue
@Path("test") // http://localhost:8083/myApp/webapi/test
public class MyResource {

/*    @PathParam("pathParam") private String pathParamExample;
    @QueryParam("query") private String queryParamExample;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String testMethod() {
        return "it works! Path param used " + pathParamExample + " and the query value: " + queryParamExample;
    }*/

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Date testMethod() {
        return Calendar.getInstance().getTime(); // Thu Dec 27 13:38:43 GMT 2018
    }
}
