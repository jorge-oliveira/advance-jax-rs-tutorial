package main.java.pt.jorge.javabrains.rest;

import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Calendar;

@Provider
public class MyDateConverterProvider implements ParamConverterProvider {
    @Override
    public <T> ParamConverter<T> getConverter(final Class<T> rowType, Type genericType, Annotation[] annotations) {

        if (rowType.getName().equals(MyDate.class.getName())) {
            return new ParamConverter<T>() {

                @Override
                public T fromString(String value) {
                    Calendar requestedDate = Calendar.getInstance();
                    if ("tommorrow".equalsIgnoreCase(value)) {
                        requestedDate.add(Calendar.DATE, 1);
                    }else if ("yesterday".equalsIgnoreCase(value)){
                        requestedDate.add(Calendar.DATE, -1);
                    }
                    MyDate myDate = new MyDate();
                    myDate.setDate(requestedDate.get(Calendar.DATE));
                    myDate.setDate(requestedDate.get(Calendar.MONTH));
                    myDate.setDate(requestedDate.get(Calendar.YEAR));
                    return rowType.cast(myDate); // cast to a type
                }

                @Override
                public String toString(T myBean) {
                    if (myBean == null){
                        return null;
                    }
                    return myBean.toString();
                }
            };
        }
        return null;
    }
}
